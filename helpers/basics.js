var basicsModule = function(){

    //makes a selection on a given Select element
    this.select = function(selector, num){
        var options = selector.all(by.tagName('option'))   
        .then(function(options){
            if(options[num]){
                options[num].click();
            } else {
                console.error('Basics.select: No option[' + num + '] for selected element. Element has ' + options.length + ' options');
            }
        });
    }

    //makes a random selection on a given Select element
    this.selectRandom = function(selector){
        var options = selector.all(by.tagName('option'))   
        .then(function(options){
            var num = this.getRandomNum(0, options.length);
            if(options[num]){
                options[num].click();
            } else {
                console.error('Basics.select: No option[' + num + '] for selected element. Element has ' + options.length + ' options');
            }
        });
    }


    //function to test whether a link redirects correctly
    this.checkLink = function(element, desiredUrl, timeout){
        element.click();
        browser.sleep(timeout);
        expect(browser.getCurrentUrl()).toEqual(desiredUrl);
    }

    //function to check link based on dynamic element
    this.checkLinkDynamic = function(element, desiredElement, elementState, timeout){
        element.click();
        browser.sleep(timeout);
        expect(desiredElement.isPresent()).toBe(elementState);
    }

    //return true if text is present in the element
    this.elementHasText = function(element, text){
        element.getText().then(
            (actualText) => {
                return (actualText.indexOf(text) !== -1);
            }
        )
    }

    //Gets a random number between min and max
    this.getRandomNum = function(min, max){
        return parseInt(Math.random() * (max - min) + min);
    };

    //Gets a random number string, each character's int value between min and max, of length
    this.getRandomNumWithLength = function(min, max, length){
        var string = "";
        for (i=0; i<length; i++){
            string+=parseInt(Math.random() * (max - min) + min);
        }
        return string;
    };

    //gets random string w/o numbers
    this.getRandomString = function(length) {
        var string = '';
        var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' //Include numbers if you want
        for (i = 0; i < length; i++) {
            string += letters.charAt(Math.floor(Math.random() * letters.length));
        }
        return string;
    }

    //gets random string w/ numbers
    this.getRandomStringWithNumbers = function(length) {
        var string = '';
        var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789' //Include numbers if you want
        for (i = 0; i < length; i++) {
            string += letters.charAt(Math.floor(Math.random() * letters.length));
        }
        return string;
    }

    this.getRandomEmail = function(length1, length2)
    {
        var domains = ['.com', '.net', '.io', '.biz', '.org', '.gov', '.co.uk', '.aus', '.ca'];
        var text = "";
        var text2 = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var domain = domains[this.getRandomNum(0, domains.length)];
        for( var i=0; i < length1; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        for( var i=0; i < length2; i++ )
            text2 += possible.charAt(Math.floor(Math.random() * possible.length));
        text += '@' + text2;
        return text + domain;
    }

}

module.exports = basicsModule;