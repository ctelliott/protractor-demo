//RequestInformation Dynamically generated Page Object File
//This is a test page for testing protractor vs LEAPWORK.

var basicModule = require('../helpers/basics');
var RequestInformation = function() {
    this.common = new basicModule();
//Page elements found
   this.degreeType = element(By.id("type-of-degree"));
   this.degree = element(By.id("degree-study"));
   this.firstName = element(By.id("firstname"));
   this.lastName = element(By.id("lastname"));
   this.email = element(By.id("email"));
   this.phone = element(By.id("email"));
   this.requestInformation = element(By.xpath('//*[@id="submit"]/button'));
   this.applyNow = element(By.xpath('//*[@id="form-apply2"]'));

   this.selectdegreeType = function(option){
       this.common.select(this.degreeType, option);
   }

   this.selectdegree = function(option){
       this.common.select(this.degree, option);
   }

   this.typefirstName = function(keys){
       this.firstName.sendKeys(keys);
   }

   this.typelastName = function(keys){
       this.lastName.sendKeys(keys);
   }

   this.typeemail = function(keys){
       this.email.sendKeys(keys);
   }

   this.typephone = function(keys){
       this.phone.sendKeys(keys);
   }

   this.clickrequestInformation = function(){
       this.requestInformation.click();
   }

   this.clickapplyNow = function(){
       this.applyNow.click();
   }

   this.fillFormWithGoodData = function(){
        this.selectdegree(1);
        this.selectdegreeType(1);
        this.typefirstName(this.common.getRandomString(5));
        this.typelastName(this.common.getRandomString(5));
        this.typeemail(this.common.getRandomEmail());
        this.typephone(this.common.getRandomNumWithLength(0,9,10));
        this.clickapplyNow();
   }

}

module.exports = RequestInformation;

