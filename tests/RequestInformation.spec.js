var RequestInformation = require('../pages/RequestInformation');
var basicsModule = require('../helpers/basics');

describe('Protractor Demo App', function() {
    browser.waitForAngularEnabled(false);
    browser.get('https://test.liberty.edu/online/microsites/online-at-liberty/');
    
    var basics = new basicsModule();
    var page = new RequestInformation();

    it('Tests good data submission pg 2', () => {
        page.fillFormWithGoodData();
        var successElement = element(By.xpath('/html/body/div[3]/p[1]/text()'));
        expect(successElement.isDisplayed()).toBeTruthy();
    });

});

