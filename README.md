# Protractor-Demo

This is a starter repo for a Protractorjs test suite.

##Installation


###Dependencies
* Node and NPM
* Selenium Webdriver (included with protractor npm install)
* ProtractorJS

```
npm install -g protractor
```


####Clone this repository
```
git clone https://ctelliott@bitbucket.org/ctelliott/protractor-demo.git
```

####Navigate to the new directory
```
cd protractor-demo
```

####Launch selenium webdriver
```
webdriver-manager update
```
```
webdriver-manager start
```

####Start the protractor test
```
Protractor conf.js
```