// conf.js
var reporters = require('jasmine-reporters')

exports.config = {
    framework: 'jasmine',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    multiCapabilities: [{
      'browserName': 'chrome'
    }],
    specs: ['./tests/RequestInformation.spec.js'],
      // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
      showColors: true, // Use colors in the command line report.
      defaultTimeoutInterval: 1240000000
    },
    onPrepare: function() {
      // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:\
      var junitReporter = new reporters.JUnitXmlReporter({
        savePath: 'reports/',
        consolidateAll: false
      });
      jasmine.getEnv().addReporter(junitReporter);

    }
  }